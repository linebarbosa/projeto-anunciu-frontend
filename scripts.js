let usuario = JSON.parse(localStorage.getItem('usuario'));
let token = localStorage.getItem('token');
let anuncios = [];

mudarPagina(0);

// document.querySelector('.login button').onclick = login;
// document.querySelector('.cadastrar button').onclick = mudarPagina(1);

function mudarPagina(numero){

  if(numero == 0){
    localStorage.clear();
  }
  
  let paginas = document.querySelectorAll('.pagina');

  for(let pagina of paginas){
    pagina.classList.remove('ativa');
  }

  paginas[numero].classList.add('ativa');
}

function login(){
  let dados = {
    cpf: document.querySelector('[name=cpf]').value,
    senha: document.querySelector('[name=senha]').value
  };

  fetch('http://localhost:8080/usuario/login', {
    body: JSON.stringify(dados),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(resposta => {
    token = resposta.headers.get('Authorization');
    localStorage.setItem('token', token);

    return resposta.json();
  }).then(dados => {
    usuario = dados;
    localStorage.setItem('usuario', JSON.stringify(usuario));

    document.querySelector('#nomeCliente').innerHTML = dados.nome;
    carregarAnuncios();
    mudarPagina(2);
  });
}

function carregarAnuncios(){
  fetch('http://localhost:8080/anuncio/consultar', {
    method: 'GET',
    headers: {
      'Authorization': token
    }
  }).then(resposta => {
    return resposta.json();
  }).then(dados => {
    anuncios = dados;

    let lista = document.querySelector('.anuncios ul');

    for(let anuncio of anuncios){
      let newAnuncio = document.createElement("a");
      newAnuncio.href = "#";
      let newLi = document.createElement("li");
      newLi.id = `${anuncio.id}`;
      
      newAnuncio.innerHTML = `${anuncio.titulo} <br> ${anuncio.descricao} <br> ${anuncio.valor}`;
      newLi.appendChild(newAnuncio);

      lista.appendChild(newLi);
      localStorage.setItem('idAnuncio', anuncio.id);
      newLi.onclick = selecaoAnuncio;
    }
  });
}

function selecaoAnuncio(){
  console.log('passeiAqui');
  fetch('http://localhost:8080/anuncio/{anuncio.id}', {
    method: 'GET',
    headers: {
      'Authorization': token
    }
  }).then(resposta => {
    return resposta.json();
  }).then(dados => {
    anuncios = dados;

    let anuncioSelecionado = document.querySelector('.anuncioSelecionado ul');

    anuncioSelecionado.innerHTML += `<li>${anuncio.titulo}</li>`;
    anuncioSelecionado.innerHTML += `${anuncio.descricao}<br>`;
    anuncioSelecionado.innerHTML += `${anuncio.categoria}<br>`;
    anuncioSelecionado.innerHTML += `${anuncio.valor}<br>`;
    anuncioSelecionado.innerHTML += `${anuncio.dataCadastro}`;
    mudarPagina(2);
  });
}

function cadastrar(){
  let dados = {
    nome: document.querySelector('[name=nome]').value,
    funcional: document.querySelector('[name=funcional]').value,
    email: document.querySelector('[name=email]').value,
    cep: document.querySelector('[name=cep]').value,
    endereco: document.querySelector('[name=endereco]').value,
    cpf: document.querySelector('[name=cpfCadastro]').value,
    senha: document.querySelector('[name=senhaCadastro]').value
  };

  fetch('http://localhost:8080/usuario', {
    body: JSON.stringify(dados),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(resposta => {
    return resposta.json();
  }).then(dados => {
    usuario = dados;
    localStorage.setItem('usuario', JSON.stringify(usuario));

    document.querySelector('#nomeCliente').innerHTML = dados.nome;
    carregarAnuncios();
    mudarPagina(2);
  });
}